# 多线程
import threading
import keyboard

# loop1 解析mjpeg http流 https://blog.csdn.net/Barry_J/article/details/101280263
import requests
import numpy as np
import builtins
import urllib.request

# loop3 modelscope框架神经网络 https://modelscope.cn/models/damo/cv_convnext-base_image-classification_garbage/summary
from modelscope.models import Model
from modelscope.pipelines import pipeline
from modelscope.utils.constant import Tasks
import time

# loop2 opencv
import cv2
from vis import cv2AddChineseText

# def loop1():
#     '''esp32cam image mjpeg'''
#     global img
#     r = requests.get(url+'cam.mjpeg', stream=True)
#     if(r.status_code == 200):
#         bytes = builtins.bytes()
#         for chunk in r.iter_content(chunk_size=1024):
#             bytes += chunk
#             a = bytes.find(b'\xff\xd8')
#             b = bytes.find(b'\xff\xd9')
#             if a != -1 and b != -1:
#                 jpg = bytes[a:b+2]
#                 bytes = bytes[b+2:]
#                 img = cv2.imdecode(np.fromstring(jpg, dtype=np.uint8), cv2.IMREAD_COLOR)
#             if exit_flag:
#                 break
#     else:
#         print("Received unexpected status code {}".format(r.status_code))
#     print('loop1 exit normally')

def loop1():
    '''esp32cam image jpg'''
    global img
    while not exit_flag:
        imgResp=urllib.request.urlopen(url+'cam-hi.jpg')
        imgNp=np.array(bytearray(imgResp.read()),dtype=np.uint8)
        img=cv2.imdecode(imgNp,-1)
    print('loop1 exit normally')

def loop2():
    '''opencv processing'''
    while not exit_flag:
        # img_puttext = img.copy()
        text = 'garbage:' + str(first_label) + ' score:' + str(first_score)
        img_puttext = cv2AddChineseText(img, text, (20, 10), (0, 255, 0), 24)
        # cv2.imwrite('./socket/esp32cam-test.jpg', img_puttext)
        cv2.imshow('esp32cam-test',img_puttext)
        if ord('q')==cv2.waitKey(10):
            break
    cv2.destroyAllWindows()
    print('loop2 exit normally')
    
def loop3():
    '''image classification'''
    global first_score
    global first_label
    while not exit_flag:
        result = image_classification(img)
        print(result)
        first_score = result['scores'][0]
        first_label = result['labels'][0]
    print('loop3 exit normally')


if __name__ == '__main__':
    # 初始化esp32cam url
    url='http://192.168.163.148/'

    # 导入模型和pipeline
    model = Model.from_pretrained('../damo/cv_convnext-base_image-classification_garbage')
    image_classification = pipeline(Tasks.image_classification, model=model)

    # 初始化img变量
    img = np.zeros((600, 800, 3), np.uint8)

    # 初始化first_score和first_label变量
    first_score = np.float32(1)
    first_label = str('')
    
    # 初始化控制线程正常退出的标志位
    exit_flag = False

    # 创建线程
    t1 = threading.Thread(target=loop1)
    t2 = threading.Thread(target=loop2)
    t3 = threading.Thread(target=loop3)
    # 启动线程
    t1.start()
    t2.start()
    t3.start()
    # 捕获用户输入 q 
    keyboard.wait('q')
    exit_flag = True
    # 等待线程结束
    t1.join()
    t2.join()
    t3.join()
    print('main thread exit normally')