import cv2
def vis_det_img(img, result):
    '''可视化目标检测框 https://blog.csdn.net/weixin_47869094/article/details/134585539'''
    def draw_label_type(draw_img,bbox,label:str,label_color=(255, 0, 255),label_size=0.5):
        # label = str(bbox[-1])
        labelSize = cv2.getTextSize(label + '0', cv2.FONT_HERSHEY_SIMPLEX, label_size, 2)[0]
        if bbox[1] - labelSize[1] - 3 < 0:
            cv2.rectangle(draw_img,
                        (bbox[0], bbox[1] + 2),
                        (bbox[0] + labelSize[0], bbox[1] + labelSize[1] + 3),
                        color=label_color,
                        thickness=-1
                        )
            cv2.putText(draw_img, label,
                        (bbox[0], bbox[1] + labelSize + 3),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        label_size,
                        (0, 0, 0),
                        thickness=1
                        )
        else:
            cv2.rectangle(draw_img,
                        (bbox[0], bbox[1] - labelSize[1] - 3),
                        (bbox[0] + labelSize[0], bbox[1] - 3),
                        color=label_color,
                        thickness=-1
                        )
            cv2.putText(draw_img, label,
                        (bbox[0], bbox[1] - 3),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        label_size,
                        (0, 0, 0),
                        thickness=1
                        )
            
    def draw_label_type_bottom(draw_img,bbox,label:str,label_color=(255, 0, 255),label_size=0.5):
        # label = str(bbox[-1])
        labelSize = cv2.getTextSize(label + '0', cv2.FONT_HERSHEY_SIMPLEX, label_size, 2)[0]
        if bbox[3] - labelSize[1] - 3 < 0:
            cv2.rectangle(draw_img,
                        (bbox[0], bbox[3] + 2),
                        (bbox[0] + labelSize[0], bbox[3] + labelSize[1] + 3),
                        color=label_color,
                        thickness=-1
                        )
            cv2.putText(draw_img, label,
                        (bbox[0], bbox[3] + labelSize + 3),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        label_size,
                        (0, 0, 0),
                        thickness=1
                        )
        else:
            cv2.rectangle(draw_img,
                        (bbox[0], bbox[3] - labelSize[1] - 3),
                        (bbox[0] + labelSize[0], bbox[3] - 3),
                        color=label_color,
                        thickness=-1
                        )
            cv2.putText(draw_img, label,
                        (bbox[0], bbox[3] - 3),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        label_size,
                        (0, 0, 0),
                        thickness=1
                        )
    
    def get_color(idx):
        idx = (idx + 1) * 3
        color = ((10 * idx) % 255, (20 * idx) % 255, (30 * idx) % 255)
        return color
    
    for i in range(len(result['labels'])):
        box = result['boxes'][i]
        box = box.astype('int')
        label_upper = str(result['labels'][i])
        label_bottom = str(result['scores'][i])
        # unique_label = list(set(result['labels']))
        # color = get_color(unique_label.index(label))

        box_color = (255, 0, 255)
        cv2.rectangle(img, (box[0], box[1]), (box[2], box[3]), color=box_color, thickness=2)
        draw_label_type(draw_img=img, bbox=box, label=label_upper, label_color=box_color, label_size=0.5)
        draw_label_type_bottom(draw_img=img, bbox=box, label=label_bottom, label_color=box_color, label_size=0.5)
    return img

import numpy as np
from PIL import Image, ImageDraw, ImageFont
def cv2AddChineseText(img, text, position, textColor=(0, 255, 0), textSize=30):
    '''cv2.putText()显示中文 https://blog.csdn.net/hijacklei/article/details/116010860'''
    if (isinstance(img, np.ndarray)):  # 判断是否OpenCV图片类型
        img = Image.fromarray(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
    # 创建一个可以在给定图像上绘图的对象
    draw = ImageDraw.Draw(img)
    # 字体的格式
    fontStyle = ImageFont.truetype(
        "simsun.ttc", textSize, encoding="utf-8")
    # 绘制文本
    draw.text(position, text, textColor, font=fontStyle)
    # 转换回OpenCV格式
    return cv2.cvtColor(np.asarray(img), cv2.COLOR_RGB2BGR)