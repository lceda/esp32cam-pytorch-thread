# 多线程
import threading
import keyboard

# esp32cam及opencv
import urllib.request
import cv2
import numpy as np
import urllib.request

# modelscope框架神经网络
from modelscope.models import Model
from modelscope.pipelines import pipeline
from modelscope.utils.constant import Tasks

def loop1():
    '''esp32cam image'''
    global img
    while not exit_flag:
        imgResp=urllib.request.urlopen(url)
        imgNp=np.array(bytearray(imgResp.read()),dtype=np.uint8)
        img=cv2.imdecode(imgNp,-1)
    print('loop1 exit normally')

def loop2():
    '''opencv processing'''
    while not exit_flag:
        cv2.imshow('test',img)
        if ord('q')==cv2.waitKey(10):
            break
    print('loop2 exit normally')
    cv2.destroyAllWindows()
    
def loop3():
    '''image classification'''
    while not exit_flag:
        result = image_classification(img)
        print(result)
    print('loop3 exit normally')

if __name__ == '__main__':
    # 初始化esp32cam url
    url='http://192.168.163.148/cam-hi.jpg'

    # 导入模型和pipeline
    model = Model.from_pretrained('../damo/cv_convnext-base_image-classification_garbage')
    image_classification = pipeline(Tasks.image_classification, model=model)

    # 初始化img变量
    imgResp=urllib.request.urlopen(url)
    imgNp=np.array(bytearray(imgResp.read()),dtype=np.uint8)
    img=cv2.imdecode(imgNp,-1)

    # 初始化控制线程正常退出的标志位
    exit_flag = False

    # 创建线程
    t1 = threading.Thread(target=loop1)
    t2 = threading.Thread(target=loop2)
    t3 = threading.Thread(target=loop3)
    # 启动线程
    t1.start()
    t2.start()
    t3.start()
    # 捕获用户输入 q 
    keyboard.wait('q')
    exit_flag = True
    # 等待线程结束
    t1.join()
    t2.join()
    t3.join()
    print('main thread exit normally')